package exec.Interfaces;

public interface ZoomListener {
	public void onZoomChanged(int zoomLevel);
}
