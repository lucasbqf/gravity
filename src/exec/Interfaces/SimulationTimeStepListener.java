package exec.Interfaces;

public interface SimulationTimeStepListener {
	public void onSimulationTimeChanged(int simulationTimeStep);
}
