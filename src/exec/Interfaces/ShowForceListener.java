package exec.Interfaces;

public interface ShowForceListener {
	public void onToggleShowForce (boolean isShowForce);
}
