package exec.Interfaces;

public interface TimeListener {
	public void onPause();
	public void onUnpause();
	public void onTimeChange(int time);
}
