package exec.Interfaces;

public interface ForceApplyListener {
	public void onApplyForce(int id ,double angle,double force);
}
