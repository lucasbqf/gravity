package exec;

import java.lang.reflect.InvocationTargetException;

import exec.MainThread;

public class Main {
	public static void main(String[] args) throws InvocationTargetException, InterruptedException {
		MainThread thread = new MainThread();
		
		Thread handler = new Thread(thread);
		handler.start();
	}

}
