package exec.screen.screenParts.optionsConteiner;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import exec.Interfaces.DeleteObjListener;
import exec.Interfaces.ListSelectListener;
import exec.screen.drawObjects.DrawPoint;

public class ListPane extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<ListSelectListener> listSelectListeners;
	DeleteObjListener deleteObjListener;
	JLabel lTitle;
	JList<String> listObj;
	DefaultListModel<String> defaultList;
	ArrayList<DrawPoint> newList;
	JScrollPane sScrollPane;
	JButton bDelete;

	public ListPane() {
		setLayout(new BorderLayout());

		listSelectListeners = new ArrayList<>();

		lTitle = new JLabel("Objetos no Universo");
		defaultList = new DefaultListModel<>();
		newList = new ArrayList<>();
		listObj = new JList<>(defaultList);
		sScrollPane = new JScrollPane(listObj);
		bDelete = new JButton("APAGAR");
		
		
		add(lTitle, BorderLayout.NORTH);
		add(sScrollPane, BorderLayout.CENTER);
		add(bDelete, BorderLayout.SOUTH);
		
		bDelete.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				for(DrawPoint point: newList){
					if(point.toString().equals(listObj.getSelectedValue())){
						deleteObjListener.onSelectToDelete(point.getId());
						break;
					}
				}
				
				
			}
		});
		listObj.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				listSelected();

			}
		});
	}

	public void addListSelectListener(ListSelectListener listSelectListener) {
		this.listSelectListeners.add(listSelectListener);

	}

	
	public void listSelected() {
		// System.out.println(listObj.getSelectedValue());
		for (DrawPoint obj : newList) {

			if (obj.toString().equals(listObj.getSelectedValue())) {
				for (ListSelectListener listener : listSelectListeners) {
					listener.onSelectOnList(obj.getId());
				}
				break;
			}
		}

	}
	
	
	@SuppressWarnings("unchecked")
	public void setList(ArrayList<DrawPoint> list) {
		// System.out.println(newList.equals(list)
		// +"|"+newList.size()+"|"+list.size());
		// System.out.println(defaultList.toString());
		while (defaultList.size() != list.size()) {
			if (newList.size() != list.size()) {
				newList = (ArrayList<DrawPoint>) list.clone();

				defaultList.clear();
				for (DrawPoint obj : newList) {
					defaultList.addElement(obj.toString());
				}
				listObj.setModel(defaultList);
				revalidate();
				repaint();
			}
		}

	}
	public void setDeletObjListener(DeleteObjListener deleteObjListener){
		this.deleteObjListener = deleteObjListener;
	}
}
