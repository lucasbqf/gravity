package exec.screen.screenParts.optionsConteiner.settingsConteiner;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import exec.Interfaces.FollowListener;
import exec.Interfaces.ShowForceListener;
import exec.Interfaces.ShowPathListener;
import exec.Interfaces.SimulationTimeStepListener;
import exec.Interfaces.TimeListener;
import exec.Interfaces.ToggleColisionListener;
import exec.Interfaces.ZoomListener;

public class SimulationOptionsPane extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel lTitle,lZoom,lGlobalWaitStep,lSimultionTimeStep;
	public JCheckBox cbCollision,cbShowPath ,cbPause,cbFollow,cbAplliedForces;
	public JSpinner sZoom;
	public JSlider sGlobalWaitStep,sSimultionTimeStep;
	
	public SimulationTimeStepListener SimTimeListener;
	public TimeListener timeListener;
	public ShowPathListener showPathListener;
	public ZoomListener zoomListener;
	public FollowListener followListener;
	public ShowForceListener showForceListener;
	public ToggleColisionListener toggleColisionListener;
	
	
	public SimulationOptionsPane() {
		setLayout(new GridLayout(0, 2));
		lTitle = new JLabel("Opcoes:");
		cbCollision = new JCheckBox("Colisoes");
		cbShowPath = new JCheckBox("Caminho");
		cbPause = new JCheckBox("Pause");
		cbFollow = new JCheckBox("seguir");
		cbAplliedForces = new JCheckBox("Forcas Aplicadas");
		lZoom = new JLabel("zoom");
		sZoom = new JSpinner();
		lGlobalWaitStep = new JLabel("Velocidade de Quadros:");
		sGlobalWaitStep = new JSlider(0,100,0);
		lSimultionTimeStep = new JLabel("Velocidade da Simula��o:");
		sSimultionTimeStep = new JSlider(1,150,10);
		sSimultionTimeStep.setMajorTickSpacing(30);
		sSimultionTimeStep.setMinorTickSpacing(10);
		sSimultionTimeStep.setPaintTicks(true);
		
		sZoom.setValue(5);
		cbShowPath.setSelected(true);
		cbCollision.setSelected(true);
		
		cbFollow.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				followListener.onToggleFollow(cbFollow.isSelected());
				
			}
		});
		
		cbShowPath.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				showPathListener.ShowPathStatus(cbShowPath.isSelected());
				
			}
		});
		cbPause.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(cbPause.isSelected()) {
					timeListener.onPause();
				}
				else
					timeListener.onUnpause();
				
			}
		});
		
		sGlobalWaitStep.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				timeListener.onTimeChange(sGlobalWaitStep.getValue()); 
				
			}
		});
		sZoom.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				zoomListener.onZoomChanged((int)sZoom.getValue());
				
			}
		});
		cbAplliedForces.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				showForceListener.onToggleShowForce(cbAplliedForces.isSelected());
				
			}
		});
		cbCollision.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				toggleColisionListener.setcollisionOption(cbCollision.isSelected());
				
			}
		});
		
		sSimultionTimeStep.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				SimTimeListener.onSimulationTimeChanged(sSimultionTimeStep.getValue());
				
			}
		});
		
		this.add(lTitle);
		this.add(new JLabel(""));
		this.add(cbCollision);
		this.add(cbShowPath);
		this.add(cbPause);
		this.add(cbFollow);
		this.add(cbAplliedForces);
		this.add(new JLabel(""));
		this.add(lZoom);
		this.add(sZoom);
		this.add(lGlobalWaitStep);
		this.add(sGlobalWaitStep);
		this.add(lSimultionTimeStep);
		this.add(sSimultionTimeStep);
		
	}
	public void setTimeListener(TimeListener timeListener) {
		this.timeListener = timeListener;
	}
	public void setShowPathListener(ShowPathListener showPathListener) {
		this.showPathListener = showPathListener;
	}
	public void setZoomListener(ZoomListener zoomListener) {
		this.zoomListener = zoomListener;
	}
	public void setFollowListener(FollowListener followListener){
		this.followListener = followListener;
	}
	public void setShowForceLisener (ShowForceListener showForceListener){
		this.showForceListener = showForceListener;
	}
	public void setToggleColisionListener(
			ToggleColisionListener toggleColisionListener) {
		this.toggleColisionListener = toggleColisionListener;
	}
	public void setSimTimeListener(SimulationTimeStepListener simTimeListener) {
		SimTimeListener = simTimeListener;
	}
}
