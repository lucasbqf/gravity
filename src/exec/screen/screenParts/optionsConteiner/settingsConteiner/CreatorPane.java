package exec.screen.screenParts.optionsConteiner.settingsConteiner;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import exec.Interfaces.NewObjectListener;

public class CreatorPane extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JLabel lTitle, lX, lY, lMass, lVolume;
	JTextField tfX, tfY, tfMass, tfVolume;
	JButton bAplly;
	NewObjectListener newObjectListener;

	public CreatorPane() {
		setLayout(new GridLayout(0, 2));
		lTitle = new JLabel("Criacao de Objetos");
		lX = new JLabel("X:");
		tfX = new JTextField();
		lY = new JLabel("Y:");
		tfY = new JTextField();
		lMass = new JLabel("Massa:");
		tfMass = new JTextField();
		lVolume = new JLabel("Volume:");
		tfVolume = new JTextField();
		bAplly = new JButton("Aplicar");

		this.add(lTitle);
		this.add(new JLabel(""));
		this.add(lX);
		this.add(tfX);
		this.add(lY);
		this.add(tfY);
		this.add(lMass);
		this.add(tfMass);
		this.add(lVolume);
		this.add(tfVolume);
		this.add(new JLabel(""));
		this.add(bAplly);

		bAplly.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try{
				newObjectListener.onNewobject(Double.parseDouble(tfX.getText()), Double.parseDouble(tfY.getText()),
						Double.parseDouble(tfMass.getText()), Double.parseDouble(tfVolume.getText()));
				}
				catch (NumberFormatException evento) {
					// TODO: handle exception
				}

			}
		});

	}

	public void setNewObjectListener(NewObjectListener newObjectListener) {
		this.newObjectListener = newObjectListener;
	}
}
