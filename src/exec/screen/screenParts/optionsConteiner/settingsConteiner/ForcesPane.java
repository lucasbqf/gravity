package exec.screen.screenParts.optionsConteiner.settingsConteiner;


import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


import exec.Interfaces.ForceApplyListener;
import exec.Interfaces.ListSelectListener;

public class ForcesPane extends JPanel implements ListSelectListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JLabel lTitle;
	JLabel lForce;
	JLabel lAngle;
	JTextField tfForce;
	JTextField tfAngle;
	JButton bAppy;
	int ID;
	
	ForceApplyListener forceApplyListener;

	public ForcesPane() {
		this.setLayout(new GridLayout(0, 2));
		lTitle = new JLabel("AplicarForcas");
		lForce = new JLabel("forca:");
		tfForce = new JTextField();
		lAngle = new JLabel("angulo:");
		tfAngle = new JTextField();
		bAppy = new JButton("aplicar");

		this.add(lTitle);
		this.add(new JLabel(""));
		this.add(lForce);
		this.add(tfForce);
		this.add(lAngle);
		this.add(tfAngle);
		this.add(new JLabel(""));
		this.add(bAppy);

		bAppy.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				forceApplyListener.onApplyForce(ID, Double.parseDouble(tfAngle.getText()), Double.parseDouble(tfForce.getText()));

			}
		});
	}

	@Override
	public void onSelectOnList(int id) {
		this.ID = id;

	}
	public void setForceApplyListener(ForceApplyListener forceApplyListener){
		this.forceApplyListener = forceApplyListener;
	}
}
