package exec.screen.screenParts.optionsConteiner;

import java.awt.GridLayout;

import javax.swing.JPanel;

import exec.screen.screenParts.optionsConteiner.settingsConteiner.CreatorPane;
import exec.screen.screenParts.optionsConteiner.settingsConteiner.ForcesPane;
import exec.screen.screenParts.optionsConteiner.settingsConteiner.SimulationOptionsPane;

public class SettingsPane extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public CreatorPane pCreator;
	public ForcesPane pForces;
	public SimulationOptionsPane pSimOpitions;
	
	public SettingsPane() {
	pCreator = new CreatorPane();
	pForces = new ForcesPane();
	pSimOpitions = new SimulationOptionsPane();
	this.add(pCreator);
	this.add(pForces);
	this.add(pSimOpitions);
	this.setLayout(new GridLayout(0, 1));
	}
}
