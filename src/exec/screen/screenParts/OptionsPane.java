package exec.screen.screenParts;

import java.awt.GridLayout;

import javax.swing.JPanel;

import exec.screen.screenParts.optionsConteiner.ListPane;
import exec.screen.screenParts.optionsConteiner.SettingsPane;

public class OptionsPane extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ListPane pList;
	public SettingsPane pSettings;
	
	public OptionsPane() {
	setLayout(new GridLayout(0, 1));
	pSettings = new SettingsPane();
	pList = new ListPane();
	this.add(pSettings);
	this.add(pList);
	}
}
