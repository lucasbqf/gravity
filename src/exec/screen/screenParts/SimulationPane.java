package exec.screen.screenParts;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

import exec.Interfaces.DrawnListener;
import exec.Interfaces.FollowListener;
import exec.Interfaces.ForceApplyListener;
import exec.Interfaces.ListSelectListener;
import exec.Interfaces.ShowForceListener;
import exec.Interfaces.ShowPathListener;
import exec.Interfaces.ZoomListener;
import exec.engine.objects.Position2d;
import exec.screen.drawObjects.DrawPoint;
import exec.screen.drawObjects.ObjectPath;
import exec.screen.drawObjects.Point2d;
import exec.screen.drawObjects.QuadtreeSectors;

public class SimulationPane extends JPanel
		implements ShowPathListener, ZoomListener, FollowListener, ShowForceListener, ListSelectListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5563456641898097841L;
	JLabel lInfo;
	private ArrayList<DrawPoint> drawList;
	private ArrayList<ObjectPath> objectPathList;
	public ArrayList<QuadtreeSectors> sectorList;
	boolean showPath = true;
	boolean isShowForce = false;
	boolean ShowTree = true;
	boolean CanApplyForce = false;
	boolean canTraslate = false;
	public int pathLength = 50;
	boolean isFollow = false;
	public int followID = 1;
	public int zoomLevel = 200;
	public int Xanchor = 0;
	public int Yanchor = 0;
	private int rootClickX = 0, rootClickY = 0;
	private int oldXAnchor = 0, oldYAnchor = 0;
	private Dimension ScreenSize;
	public DrawnListener drawnListener;
	ForceApplyListener forceApplyListener;
	private boolean setApplyForce = false;
	private int[] posMousePressed = {0,0};
	private int[] posMouseReleased = {0,0};
	static double TWOPI = 6.2831853071795865;
	
	//private int selectedID;
	
	public void addDrawnListener(DrawnListener drawnListener) {
		this.drawnListener = drawnListener;
	}
	
	public void setDrawlist(ArrayList<DrawPoint> newDrawList, ArrayList<QuadtreeSectors> newSectorList) {
		drawList = newDrawList;
		sectorList = newSectorList;
	}

	public void setWinSize() {
		ScreenSize = this.getSize();
		Xanchor = (int) (-ScreenSize.getWidth() / 2);
		Yanchor = (int) (-ScreenSize.getHeight() / 2);
		oldXAnchor = (int) (-ScreenSize.getWidth() / 2); 
		oldYAnchor = (int) (-ScreenSize.getHeight() / 2);
		// System.out.println(Xanchor + "|" + Yanchor);
	}

	public SimulationPane() {
		setBackground(Color.BLACK);
		lInfo = new JLabel("tela X:" + Xanchor + "| tela Y" + Yanchor + "| zoom: " + zoomLevel);
		this.setLayout(new BorderLayout());
		this.add(lInfo, BorderLayout.SOUTH);
		drawList = new ArrayList<>();
		objectPathList = new ArrayList<>();
		setWinSize();
		addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				zoomLevel += e.getWheelRotation()*5;
				if (zoomLevel < 1)
					zoomLevel = 1;
				else {
					Xanchor -= e.getWheelRotation() * (ScreenSize.getWidth() / 2);
					Yanchor -= e.getWheelRotation() * (ScreenSize.getHeight() / 2);
				}

			}
		});
		addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseDragged(MouseEvent e) {
				//System.out.print("bt arrastado:"+ e.getModifiers()+ "\n"); 
				if (!isFollow && e.getModifiers() == 4) {
					Xanchor = oldXAnchor + (+rootClickX - e.getX()) * zoomLevel;
					Yanchor = oldYAnchor + (+rootClickY - e.getY()) * zoomLevel;
				}
				if(e.getModifiers() == 16){
					setApplyForce = true;
				}
			}
		});
		addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				//System.out.print(" soltou bt:"+e.getButton()+"\n");
				posMouseReleased[0]= e.getX();
				posMouseReleased[1]= e.getY();
				if(setApplyForce == true){
					double angle = Math.atan2(posMousePressed[1] - posMouseReleased[1], posMousePressed[0] - posMouseReleased[0]);
					if (angle < 0.0)
						angle += TWOPI;
					double angleToApplyForce = Math.toDegrees(angle);
					double forceToApplyForce = Math.sqrt(Math.pow((posMouseReleased[0] - posMousePressed[0]), 2) + Math.pow((posMouseReleased[1] - posMousePressed[1]), 2));
					double normalizedForce =0;
					for(DrawPoint objDesenho : drawList ) {
						if(followID == objDesenho.getId()) {
							normalizedForce = (forceToApplyForce /100) * objDesenho.getSize();
						}
					}
					
					forceApplyListener.onApplyForce(followID, angleToApplyForce, normalizedForce );
					System.out.print(" aplicou em: ID:"+followID+"|angulo:"+ angleToApplyForce +"|for�a:"+normalizedForce +"\n");
					setApplyForce = false ;
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				//System.out.print("bt pressionado:" + e.getButton()+"\n");
				//position to apply forces
				posMousePressed[0]= e.getX();
				posMousePressed[1]= e.getY();
				
				if (!isFollow) {
					rootClickX = e.getX();
					rootClickY = e.getY();
					oldXAnchor = Xanchor;
					oldYAnchor = Yanchor;
				}
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				//System.out.print("bt clickado:" + e.getButton() + "\n");
				/** return the ID from thle clicked Object in the SimPane**/
				for (DrawPoint point : drawList){
					point.getX();
					double halfsize = (point.getSize() / zoomLevel);
					double relativeX = (( point.getX() - Xanchor) / zoomLevel - halfsize);
					double relativeY = ((point.getY() - Yanchor) / zoomLevel - halfsize);
					if ((e.getX() > relativeX ) && (e.getX() < relativeX +(point.getSize() / zoomLevel) * 2)) {
						if((e.getY()> relativeY) && (e.getY()< relativeY +(point.getSize() / zoomLevel) * 2)){
							//System.out.println("ID clicado:"+point.getId() );
							followID = point.getId();
						}
					}
				}
			}
				
		});
	}

	public void Follow(double objX, double objY) {
		Xanchor = (int) ((objX) - (ScreenSize.getWidth() / 2) * zoomLevel);
		Yanchor = (int) ((objY) - (ScreenSize.getHeight() / 2) * zoomLevel);
	}

	public void paintComponent(Graphics g) {
		long time = System.currentTimeMillis();
		super.paintComponent(g);
		ScreenSize = this.getSize();
		/**
		 * this is for barnesHut implementation**/
		if (ShowTree) {
			g.setColor(Color.green);
			if (sectorList != null) {
				for (QuadtreeSectors sector : sectorList) {
					g.setColor(sector.getCor());
					g.drawRect((int) ((sector.getBgX() - Xanchor) / zoomLevel),
							(int) ((sector.getBgY() - Yanchor) / zoomLevel),
							(int) ((sector.getEndX() - sector.getBgX()) / zoomLevel),
							(int) ((sector.getEndY() - sector.getBgY()) / zoomLevel));
					if (!sector.isRealObj()) {
						g.fillOval((int) ((sector.getFakeObjX() - Xanchor - 300) / zoomLevel),
								(int) ((sector.getFakeObjY() - Yanchor - 300) / zoomLevel), (int) ((600 / zoomLevel)),
								(int) ((600 / zoomLevel)));
						
					}
				}
			}
		}
		for (DrawPoint point : drawList) {
			/**
			 * this part is to make the camera follow one object,if the flag is set
			 **/
			if (isFollow && point.getId() == followID) {
				Follow(point.getX(), point.getY());
			}
			if(point.getId() == followID){
				g.setColor(Color.blue);
			}else
				g.setColor(Color.white);
			
			/** draw a circle shape where the objects are **/
			double halfsize = (point.getSize() / zoomLevel);
			if(halfsize > 1)
			g.fillOval((int) ((point.getX() - Xanchor) / zoomLevel - halfsize),
					(int) ((point.getY() - Yanchor) / zoomLevel - halfsize), (int) ((point.getSize() / zoomLevel) * 2),
					(int) ((point.getSize() / zoomLevel) * 2));

			/** puts the momentum vector on screen if the flag is set **/
			if (isShowForce) {
				g.setColor(Color.blue);
				g.drawLine((int) ((point.getX() + (point.getMomentumX() * 30) - Xanchor) / zoomLevel),
						(int) ((point.getY() + (point.getMomentumY() * 30) - Yanchor) / zoomLevel),
						(int) ((point.getX() - Xanchor) / zoomLevel), (int) ((point.getY() - Yanchor) / zoomLevel));
			}

			if (showPath) {
				boolean find = false;
				for (ObjectPath path : objectPathList) {
					if (point.getId() == path.id) {

						find = true;
						path.PointList.add(new Point2d(point.getX(), point.getY()));
						if (path.PointList.size() > pathLength) {
							path.PointList.remove(0);
						}
						Point2d de = null;
						for (Point2d points : path.PointList) {

							if (de != null) {

								g.setColor(Color.RED);
								g.drawLine((int) ((de.getX() - Xanchor) / zoomLevel),
										(int) ((de.getY() - Yanchor) / zoomLevel),
										(int) ((points.getX() - Xanchor) / zoomLevel),
										(int) ((points.getY() - Yanchor) / zoomLevel));
							}

							de = points;
						}
						break;
					}
				}
				if (!find) {
					objectPathList.add(new ObjectPath(point.getId()));
					objectPathList.get(objectPathList.size() - 1).PointList
							.add(new Point2d(point.getX(), point.getY()));
				}

			}

		}
		// lInfo.setText("tela X:" + Xanchor + "| tela Y" + Yanchor + "| zoom: " +
		// zoomLevel);
		lInfo.setText("tela X:" + (Xanchor + (ScreenSize.getWidth() / 2) * zoomLevel) + "| tela Y"
				+ (Yanchor + (ScreenSize.getHeight() / 2) * zoomLevel) + "| zoom: " + zoomLevel);
		g.drawRect((int) (ScreenSize.getWidth() / 2) - 10, (int) (ScreenSize.getHeight() / 2) - 10, 20, 20);
		
		time =time - System.currentTimeMillis() ;
		drawnListener.ondrawnFinished(time);
	}

	@Override
	public void ShowPathStatus(boolean isShowingPath) {
		this.showPath = isShowingPath;
		objectPathList.clear();

	}

	@Override
	public void onZoomChanged(int zoomLevel) {
		this.zoomLevel = zoomLevel;

	}

	@Override
	public void onToggleFollow(boolean isFollow) {
		// System.out.println("follow=" + isFollow);
		this.isFollow = isFollow;

	}

	@Override
	public void onToggleShowForce(boolean isShowForce) {
		this.isShowForce = isShowForce;

	}

	@Override
	public void onSelectOnList(int id) {
		followID = id;

	}
	public void setForceApplyListener(ForceApplyListener forceApplyListener){
		this.forceApplyListener = forceApplyListener;
	}
}
