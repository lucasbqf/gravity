package exec.screen;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JSplitPane;

import exec.screen.drawObjects.DrawPoint;
import exec.screen.drawObjects.QuadtreeSectors;
import exec.screen.screenParts.OptionsPane;
import exec.screen.screenParts.SimulationPane;

public class MainScreen extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public SimulationPane simPane;
	public OptionsPane pOptions;
	public Boolean drawn = false;
	public MainScreen(){
		super("Gravity");
		this.setLayout(new BorderLayout());
		simPane = new SimulationPane();
		pOptions = new OptionsPane();
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,pOptions,simPane);
		splitPane.setOneTouchExpandable(true);
		splitPane.setDividerLocation(300);
		
		
		//this.add(simPane,BorderLayout.CENTER);
		//this.add(pOptions,BorderLayout.WEST);
		
		this.add(splitPane);
		
		pOptions.pSettings.pSimOpitions.setShowPathListener(simPane);
		pOptions.pSettings.pSimOpitions.setZoomListener(simPane);
		pOptions.pSettings.pSimOpitions.setFollowListener(simPane);
		pOptions.pSettings.pSimOpitions.setShowForceLisener(simPane);
		pOptions.pList.addListSelectListener(simPane);
		pOptions.pList.addListSelectListener(pOptions.pSettings.pForces);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
		//setSize(Toolkit.getDefaultToolkit().getScreenSize());
	}
	public void setDrawList(ArrayList<DrawPoint> drawList,ArrayList<QuadtreeSectors> sectorList){
		//System.out.println("!!");
		pOptions.pList.setList(drawList);
		simPane.setDrawlist(drawList,sectorList);
		
	}
	
}
