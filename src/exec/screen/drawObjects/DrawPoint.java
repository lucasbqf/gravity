package exec.screen.drawObjects;

public class DrawPoint {
	private double  x,y;
	private int id;
	private double size;
	private double mass;
	private double momentumX,momentumY;
	
	public double getMomentumX() {
		return momentumX;
	}

	public void setMomentumX(double momentumX) {
		this.momentumX = momentumX;
	}

	public double getMomentumY() {
		return momentumY;
	}

	public void setMomentumY(double momentumY) {
		this.momentumY = momentumY;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	
	
	public DrawPoint(double x,double y,double size,int id,double momentumX, double momentumY,double mass){
		this.x = x;
		this.y = y;
		this.size = size;
		this.id = id;
		this.mass = mass;
		this.momentumX= momentumX;
		this.momentumY = momentumY;
	}
	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		
		return "ID:"+this.id+"|massa:"+this.mass+"|Volume:"+this.size; 
	}
}
