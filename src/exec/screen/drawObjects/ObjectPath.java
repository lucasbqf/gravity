package exec.screen.drawObjects;

import java.util.ArrayList;

public class ObjectPath {
	public ArrayList<Point2d> PointList;
	public int id;

	public ObjectPath(int id) {
		this.id = id;
		PointList = new ArrayList<>();
	}
}
