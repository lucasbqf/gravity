package exec.screen.drawObjects;

import java.awt.Color;


public class QuadtreeSectors {
	double bgX,endX,bgY,endY;
	double fakeObjX,fakeObjY;
	double mass;
	boolean isRealObj;
	Color cor = Color.WHITE;
	
	public Color getCor() {
		return cor;
	}


	public void setCor(Color cor) {
		this.cor = cor;
	}


	public QuadtreeSectors(double bgX,double endX,double bgY,double endY,double fakeObjX,double fakeObjY , double mass,boolean isRealObj,Color cor) {
		this.bgX =bgX;
		this.bgY = bgY;
		this.endX = endX;
		this.endY = endY;
		this.fakeObjX = fakeObjX;
		this.fakeObjY = fakeObjY;
		this.mass = mass;
		this.isRealObj = isRealObj;
		this.cor = cor;
		
		
	}
	
	
	public boolean isRealObj() {
		return isRealObj;
	}


	public void setRealObj(boolean isRealObj) {
		this.isRealObj = isRealObj;
	}


	public double getMass() {
		return mass;
	}
	public void setMass(double mass) {
		this.mass = mass;
	}
	public double getBgX() {
		return bgX;
	}
	public void setBgX(double bgX) {
		this.bgX = bgX;
	}
	public double getEndX() {
		return endX;
	}
	public void setEndX(double endX) {
		this.endX = endX;
	}
	public double getBgY() {
		return bgY;
	}
	public void setBgY(double bgY) {
		this.bgY = bgY;
	}
	public double getEndY() {
		return endY;
	}
	public void setEndY(double endY) {
		this.endY = endY;
	}
	public double getFakeObjX() {
		return fakeObjX;
	}
	public void setFakeObjX(double fakeObjX) {
		this.fakeObjX = fakeObjX;
	}
	public double getFakeObjY() {
		return fakeObjY;
	}
	public void setFakeObjY(double fakeObjY) {
		this.fakeObjY = fakeObjY;
	}
	
}
