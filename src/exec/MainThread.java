package exec;

import exec.Interfaces.DrawnListener;
import exec.Interfaces.TimeListener;
import exec.engine.Engine;
import exec.screen.MainScreen;

public class MainThread implements Runnable, TimeListener, DrawnListener {

	private static int time = 0;
	private static boolean isPaused = true;
	Engine engine;
	MainScreen screen;
	long Dtime;
	public void run() {

		engine = new Engine();
		screen = new MainScreen();

		screen.pOptions.pSettings.pSimOpitions.setTimeListener(this);
		screen.pOptions.pSettings.pCreator.setNewObjectListener(engine);
		screen.pOptions.pSettings.pSimOpitions.setSimTimeListener(engine);
		screen.pOptions.pSettings.pForces.setForceApplyListener(engine);
		screen.pOptions.pSettings.pSimOpitions.setToggleColisionListener(engine);
		screen.pOptions.pList.setDeletObjListener(engine);
		screen.simPane.addDrawnListener(this);
		screen.simPane.setForceApplyListener(engine);
		newCicle();
		//while (true) {

			// try {
			// Thread.sleep(time);
			// } catch (InterruptedException ex) {
			// Thread.currentThread().interrupt();
			// }
		//}
	}

	public void newCicle() {
		long time = System.currentTimeMillis();
		engine.update(isPaused);
		screen.setDrawList(engine.getDrawableList(), engine.getsectorList());
		long cTime = System.currentTimeMillis() - time;
		//System.out.println("TC="+cTime +"|TD= "+ Dtime +"|TT="+ (cTime + Dtime)+"|FPS ="+(1000f/(cTime + Dtime)));
		screen.repaint();
		engine.cleanUp();
		
	}

	@Override
	public void onPause() {
		isPaused = true;

	}

	@Override
	public void onUnpause() {
		isPaused = false;

	}

	@Override
	public void onTimeChange(int theTime) {
		time = theTime;
	}

	@Override
	public void ondrawnFinished(long time) {
		
		Dtime = MainThread.time - time;
		//System.out.println("TEMPO DE DESENHO="+ Dtime);
		if (Dtime > 0) {
			try {
				Thread.sleep(Dtime);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}
		newCicle();
	}

}
