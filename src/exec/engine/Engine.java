package exec.engine;

import java.util.ArrayList;
import java.util.Random;

import exec.Interfaces.DeleteObjListener;
import exec.Interfaces.ForceApplyListener;
import exec.Interfaces.NewObjectListener;
import exec.Interfaces.SimulationTimeStepListener;
import exec.Interfaces.ToggleColisionListener;
import exec.engine.objects.Obj;
import exec.engine.quadtree.Node;
import exec.screen.drawObjects.DrawPoint;
import exec.screen.drawObjects.QuadtreeSectors;

public class Engine implements ForceApplyListener, NewObjectListener, DeleteObjListener, ToggleColisionListener,SimulationTimeStepListener {
	ArrayList<Obj> objList;
	ArrayList<DrawPoint> drawList;
	public ArrayList<QuadtreeSectors> sectorsList;
	Node universeNode;
	double simulationTimeStep = 1;
	boolean colisionType = true;

	int LoopCount = 0;

	public Engine() {
		objList = new ArrayList<>();
		drawList = new ArrayList<>();
		sectorsList = new ArrayList<>();
		universeNode = new Node();

		//randomSistem(100);
		dummieSistem();
		//twoBodysistem();

	}

	public void update(boolean isPaused) {
		if (!isPaused) {
			for (Obj object : objList) {
				object.gravity(objList, colisionType,simulationTimeStep);
			}
		}

		drawList.clear();
		for (Obj object : objList) {
			if (!isPaused  && !object.isToRemove()) {
				object.updatePositionByVelocity(simulationTimeStep);
			}
				drawList.add(new DrawPoint((object.getX()), object.getY(), object.getVolume(), object.getId(),
						object.getVelocityX(), object.getVelocityY(), object.getMass()));
				// System.out.println((object.getX())+"|" +object.getY()+"|"+ object.getId());
		}
		sectorsList.clear();
		sectorsList = universeNode.IsideNodesToList(universeNode);
		// universeNode.UpdateTree(universeNode, universeNode);
		// }
		//
		// }

	}

	public void cleanUp() {
		if (!colisionType) {
			for (Obj object : objList) {
				if (object.isToRemove() == true) {
					objList.remove(object);
				}
			}
		}
	}

	public ArrayList<DrawPoint> getDrawableList() {
		return drawList;
	}

	public ArrayList<QuadtreeSectors> getsectorList() {
		return sectorsList;
	}

	@Override
	public void onApplyForce(int id, double angle, double force) {
		for (Obj object : objList) {
			if (id == object.getId()) {
				object.forceEAngleToVelocity(angle, force);
				break;
			}
		}

	}

	@Override
	public void onNewobject(double x, double y, double mass, double volume) {
		Obj object = new Obj(x, y, mass, volume);
		objList.add(object);
		// universeNode.addObject(universeNode, universeNode, object);

	}

	@Override
	public void onSelectToDelete(int ID) {
		for (Obj obj : objList) {
			if (obj.getId() == ID) {
				objList.remove(obj);
				break;
			}
		}

	}

	@Override
	public void setcollisionOption(boolean colisionOption) {
		this.colisionType = colisionOption;

	}

	public void dummieSistem() {
		//// CRIA��O DE DUMMIES
		onNewobject(0, 0, (500000), 10000);

		Obj novo = new Obj(15000, 0, 100, 500);
		objList.add(novo);
		onApplyForce(2,90, 200);
		
		novo = new Obj(40000, 0, 100, 500);
		objList.add(novo);
		onApplyForce(3,90, 120);
		
		novo = new Obj(70000, 0, 200, 1200);
		objList.add(novo);
		onApplyForce(4,90, 175);
					
		
					
		novo = new Obj(120000, 0, 10000, 5000);
		objList.add(novo);
		onApplyForce(5,90, 6700);
		
		novo = new Obj(109000, 0, 50, 300);
		objList.add(novo);
		onApplyForce(6,90, 50);
		
		novo = new Obj(200000, 0, 20000, 7000);
		objList.add(novo);
		onApplyForce(7,90, 11000);
			
		novo = new Obj(215000, 0, 100, 500);
		objList.add(novo);
		onApplyForce(8,90, 90);
		
		novo = new Obj(175000, 0, 100, 500);
		objList.add(novo);
		onApplyForce(9,90, 90);
		// ACABOU DUMMIES
	}

	public void randomSistem(int nBodys) {
		Random rand = new Random();
		for (int x = 0; x < nBodys; x++) {
			Obj novo = new Obj((rand.nextDouble() * 100000) - 50000, (rand.nextDouble() * 100000) - 50000,
					(rand.nextDouble() * 1000) + 50, (rand.nextDouble() * 1000) + 50);
			//novo.forceEAngleToVelocity(rand.nextDouble() * 360, rand.nextDouble() * 100);
			objList.add(novo);
			// onNewobject((rand.nextDouble() * 100000) - 50000, (rand.nextDouble() *
			// 100000) - 50000,
			// (rand.nextDouble() * 100) + 50, (rand.nextDouble() * 1000) + 50);

		}
	}
	public void twoBodysistem() {
		onNewobject(0, 0, (500000), 10000);

		Obj novo = new Obj(15000, 0, 100, 500);
		objList.add(novo);
	}

	@Override
	public void onSimulationTimeChanged(int simulationTimeStep) {
		this.simulationTimeStep = ((double)simulationTimeStep)/10;
		
	}
}
