package exec.engine.objects;

public class Position2d {
	private double x,y;
	public Position2d(double x, double y ){
		this.x = x;
		this.y = y;
	}
	public Position2d(){
		this.x = 0;
		this.y = 0;
	}
	public void setPosition(double x, double y){
		this.x = x;
		this.y = y;
		
	}
	public void setPosition(double[] position){
		this.x = position[0];
		this.y = position[1];
		
	}
	public void setX(double x){
		this.x = x;
	}
	public void setY(double y){
		this.y = y;
	}
	public double getX(){
		return x;
	}
	public double getY(){
		return y;
	}
	public double[] getPosition(){
		double[] posicao = new double[2];
		posicao[0]=x;
		posicao[1]=y;
		return posicao;
	}
}
