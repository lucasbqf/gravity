package exec.engine.objects;

import java.util.List;

public class Obj {
	private Position2d position;
	private static int counter;
	private static double GRAVITATIONAL_CONSTANT = 6.67;
	static double TWOPI = 6.2831853071795865;
	private int id;
	private double velocity[] = { 0, 0 };

	private double mass; // mass in kg
	private double volume;// radius in m
	private double density;
	private double hardness = 1;
	private boolean toRemove = false;
	//private double drag = 0;

	public Obj() {
		setId(counter);
		counter += 1;
		position = new Position2d();
		mass = 100;
		volume = mass;
		setDensity();
	}

	public Obj(double x, double y, double mass) {
		setId(counter);
		counter += 1;
		this.mass = mass;
		position = new Position2d(x, y);
		volume = this.mass;
		setDensity();
	}

	public Obj(double x, double y, double mass, double volume) {
		setId(counter);
		counter += 1;
		this.mass = mass;
		this.volume = volume;
		position = new Position2d(x, y);
		setDensity();
	}

	public void updatePositionByVelocity(double timestep) {
		position.setPosition(getX() + (velocity[0]*timestep), getY() + (velocity[1]*timestep));
	}

	public void colisionToVelocity(Obj object) {
		double R = this.hardness * object.hardness;
		double m21, dvx2, a, x21, y21, vx21, vy21, fy21, sign, vx_cm, vy_cm;

		m21 = object.getMass() / this.getMass();
		x21 = object.getX() - this.getX();
		y21 = object.getY() - this.getY();
		vx21 = object.velocity[0] - this.velocity[0];
		vy21 = object.velocity[1] - this.velocity[1];

		vx_cm = (this.getMass() * this.velocity[0] + object.getMass() * object.velocity[0])
				/ (this.getMass() + object.getMass());
		vy_cm = (this.getMass() * this.velocity[1] + object.getMass() * object.velocity[1])
				/ (this.getMass() + object.getMass());

		// *** return old velocities if balls are not approaching ***
		if ((vx21 * x21 + vy21 * y21) >= 0)
			return;

		// *** I have inserted the following statements to avoid a zero divide;
		// (for single precision calculations,
		// 1.0E-12 should be replaced by a larger value). **************

		fy21 = 1.0E-12 * Math.abs(y21);
		if (Math.abs(x21) < fy21) {
			if (x21 < 0) {
				sign = -1;
			} else {
				sign = 1;
			}
			x21 = fy21 * sign;
		}

		// *** update velocities ***
		a = y21 / x21;
		dvx2 = -2 * (vx21 + a * vy21) / ((1 + a * a) * (1 + m21));
		object.velocity[0] = object.velocity[0] + dvx2;
		object.velocity[1] = object.velocity[1] + a * dvx2;
		this.velocity[0] = this.velocity[0] - m21 * dvx2;
		this.velocity[1] = this.velocity[1] - a * m21 * dvx2;

		// *** velocity correction for inelastic collisions ***
		this.velocity[0] = (this.velocity[0] - vx_cm) * R + vx_cm;
		this.velocity[1] = (this.velocity[1] - vy_cm) * R + vy_cm;
		object.velocity[0] = (object.velocity[0] - vx_cm) * R + vx_cm;
		object.velocity[1] = (object.velocity[1] - vy_cm) * R + vy_cm;

	}
	private void mergeCollison(Obj object){
		colisionToVelocity(object);
		if (this.getMass()>= object.getMass()){
			this.setMass(this.getMass()+ object.getMass());
			this.volume = this.volume + (object.volume/2);
			this.setDensity();
			object.setToRemove(true);
		}		
	}

	public double velocityToForce() {
		return Math.sqrt(Math.pow(getVelocityX(), 2) + Math.pow(getVelocityY(), 2)) * this.getMass();
	}

	public double velocityToAngle() {
		double angle = Math.atan2(this.getVelocityY(), this.getVelocityX());
		if (angle < 0.0)
			angle += TWOPI;
		return Math.toDegrees(angle);
	}

	public void forceEAngleToVelocity(double angle, double force) {
		double radianAngle = Math.toRadians(angle);
		velocity[1] += (force * Math.toDegrees(Math.sin(radianAngle)) / this.getMass());
		velocity[0] += (force * Math.toDegrees(Math.cos(radianAngle)) / this.getMass());
	}

	public double angleFromObj(Obj object) {
		double angle = Math.atan2(object.getY() - this.getY(), object.getX() - this.getX());
		if (angle < 0.0)
			angle += TWOPI;
		return Math.toDegrees(angle);
	}

	public double getGravityForce(Obj object) {
	return (GRAVITATIONAL_CONSTANT * this.mass * object.getMass()) / Math.pow(getDistFromObj(object), 2);
	}

	public void gravity(List<Obj> objList ,boolean colisionType,double timestep) {
		for (Obj object : objList) {
			if (this != object && object.mass>0 && this.mass >0) {
				// if (!isCollided(object)) {
				double GravityForce = getGravityForce(object) *timestep;
				double GravityAngle = angleFromObj(object);
				this.forceEAngleToVelocity(GravityAngle, GravityForce);
				// }
				if (isCollided(object) && colisionType == true) {
					colisionToVelocity(object);
				
				}
				else if(isCollided(object) && colisionType == false){
					mergeCollison(object);
				}
			}
		}
	}

	public boolean isCollided(Obj object) {
		if (this.getDistFromObj(object) > this.getVolume() + object.getVolume()) {
			return false;
		} else {
			// System.out.println(this.getDistFromObj(object));
			return true;
		}

	}

	public double[] getVelocity() {
		return velocity;
	}

	public double getVelocityX() {
		return velocity[0];
	}

	public double getVelocityY() {
		return velocity[1];
	}

	public void setDensity() {
		density = mass / (3 / 4 * Math.PI * Math.pow(volume, 3));
	}

	public double getDensity() {
		return density;
	}

	public double getMass() {
		return mass;
	}

	public void setMass(double mass) {
		this.mass = mass;
	}

	public double getVolume() {
		return volume;
	}

	public double getDistFromObj(Obj object) {
		double dist = Math
				.sqrt(Math.pow((this.getX() - object.getX()), 2) + Math.pow((this.getY() - object.getY()), 2));
		return dist;
	}

	public double getX() {
		return this.position.getX();
	}

	public double getY() {
		return this.position.getY();
	}

	public void setposition(double x, double y) {
		this.position.setPosition(x, y);
	}

	public String toString() {

		return "X:" + getX() + "| Y:" + getY();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isToRemove() {
		return toRemove;
	}

	public void setToRemove(boolean toRemove) {
		this.toRemove = toRemove;
	}
}
