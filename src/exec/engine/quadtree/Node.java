package exec.engine.quadtree;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Vector;

import exec.engine.objects.Obj;
import exec.screen.drawObjects.QuadtreeSectors;

public class Node {
	boolean isrealObject, isFill, isExternalNode;
	int quadrant;
	Obj object;
	Node fatherNode;
	Vector<Node> insideNodes;
	double bgXBoundry;
	double endXBoundry;
	double bgYBoundry;
	double endYBoundry;
	double centralX, centralY;
	Color cor = Color.WHITE;

	// n� pai
	public Node() {
		/**
		 * cria o n� universal, s� deve ser usado uma vez!
		 */
		insideNodes = new Vector<>();
		quadrant = 0;
		isrealObject = isFill = false;
		isExternalNode = true;
		fatherNode = null;

		bgXBoundry = -(Double.MAX_VALUE - 1);
		endXBoundry = Double.MAX_VALUE - 1;
		bgYBoundry = -(Double.MAX_VALUE - 1);
		endYBoundry = Double.MAX_VALUE - 1;

		// bgXBoundry = -10000000;
		// endXBoundry = 10000000;
		// bgYBoundry = -10000000;
		// endYBoundry = 10000000;
		centralX = centralY = 0;
		object = new Obj(centralX, centralY, 0);

	}

	public Node(Node root, Node father, int quadrant) {
		/**
		 * cria um n� vazio que retira seus valores de localizacao de seu pai,
		 * dependendo do quadrante
		 **/
		insideNodes = new Vector<>();
		fatherNode = father;
		father.isExternalNode = false;
		if (quadrant == 0) {
			bgXBoundry = father.bgXBoundry;
			endXBoundry = father.centralX;
			bgYBoundry = father.bgYBoundry;
			endYBoundry = father.centralY;
		} else if (quadrant == 1) {
			bgXBoundry = father.centralX;
			endXBoundry = father.endXBoundry;
			bgYBoundry = father.bgYBoundry;
			endYBoundry = father.centralY;
		} else if (quadrant == 2) {
			bgXBoundry = father.bgXBoundry;
			endXBoundry = father.centralX;
			bgYBoundry = father.centralY;
			endYBoundry = father.endYBoundry;
		} else if (quadrant == 3) {
			bgXBoundry = father.centralX;
			endXBoundry = father.endXBoundry;
			bgYBoundry = father.centralY;
			endYBoundry = father.endYBoundry;
		}
		centralX = bgXBoundry + ((endXBoundry - bgXBoundry) / 2);
		centralY = bgYBoundry + ((endYBoundry - bgYBoundry) / 2);
		object = new Obj(centralX, centralY, 0);
		isFill = isrealObject = false;
		isExternalNode = true;
	}
	public void createSectors(Node root, Node node) {
		node.insideNodes.add(new Node(root, node, 0));
		node.insideNodes.get(0).cor = new Color((int) (Math.random() * 0x1000000));
		node.insideNodes.add(new Node(root, node, 1));
		node.insideNodes.get(1).cor = new Color((int) (Math.random() * 0x1000000));
		node.insideNodes.add(new Node(root, node, 2));
		node.insideNodes.get(2).cor = new Color((int) (Math.random() * 0x1000000));
		node.insideNodes.add(new Node(root, node, 3));
		node.insideNodes.get(3).cor = new Color((int) (Math.random() * 0x1000000));
	}

	public Vector<Double> getInfoFromInsideNodes(Node node) {
		/**
		 * verifica as informacoes de
		 */
		Vector<Double> centerData = new Vector<>();
		if (!node.isExternalNode) {
			double x = 0, y = 0, mass = 0;
			for (Node Inode : node.insideNodes) {
				if (Inode.isFill) {
					mass = mass + Inode.object.getMass();
					x = x + (Inode.object.getX() * Inode.object.getMass());
					y = y + (Inode.object.getY() * Inode.object.getMass());
				}
			}
			x = x / mass;
			y = y / mass;
			centerData.add(x);
			centerData.add(y);
			centerData.add(mass);

		}
		return centerData;
	}

	public ArrayList<QuadtreeSectors> IsideNodesToList(Node node) {
		/**
		 * recursivamente viaja pelos n�s e adiciona a uma lista de saida.
		 */
		ArrayList<QuadtreeSectors> output = new ArrayList<>();
		if (node.insideNodes.size() != 0) {
			for (Node Inode : node.insideNodes) {
				if (Inode.isFill) {
					output.add(new QuadtreeSectors(Inode.bgXBoundry, Inode.endXBoundry, Inode.bgYBoundry,
							Inode.endYBoundry, Inode.object.getX(), Inode.object.getY(), Inode.object.getMass(),
							Inode.isrealObject, Inode.cor));
				}
				output.addAll(IsideNodesToList(Inode));

			}
		}
		return output;
	}

	private int searchBestQuadrant(Node node, Obj object) {
		/**
		 * procura, e devolve o quadrante onde o objeto deve ficar
		 */

		if ((object.getX() > node.insideNodes.get(0).bgXBoundry && object.getX() < node.insideNodes.get(0).endXBoundry)
				&& (object.getY() > node.insideNodes.get(0).bgYBoundry
						&& object.getY() < node.insideNodes.get(0).endYBoundry)) {
			return 0;
		} else if ((object.getX() > node.insideNodes.get(1).bgXBoundry
				&& object.getX() < node.insideNodes.get(1).endXBoundry)
				&& (object.getY() > node.insideNodes.get(1).bgYBoundry
						&& object.getY() < node.insideNodes.get(1).endYBoundry)) {
			return 1;
		} else if ((object.getX() > node.insideNodes.get(2).bgXBoundry
				&& object.getX() < node.insideNodes.get(2).endXBoundry)
				&& (object.getY() > node.insideNodes.get(2).bgYBoundry
						&& object.getY() < node.insideNodes.get(2).endYBoundry)) {
			return 2;
		} else
			return 3;
	}
//________________________________________________________________________________
//___________________________ daqui pra frente pode ter bugs!_____________________
	
	public void UpdateTree(Node root, Node node) {
		if ((node.object.getX() < node.bgXBoundry || node.object.getX() > node.endXBoundry
				|| node.object.getY() < node.bgYBoundry || node.object.getY() > node.endYBoundry)) {
			// System.out.println("update no n� "+ node.toString());
			addObject(root, root, node.object);
			node.object = new Obj(node.centralX, node.centralY, 0);
			node.isrealObject = isFill = false;
			node.recalculateIntenalNode(node.fatherNode);
		}
		if (!isExternalNode) {
			for (Node Inode : node.insideNodes) {
				UpdateTree(root, Inode);
			}
		}
	}

	public void recalculateIntenalNode(Node node) {
		//System.out.println("recalculando n� " + node.toString());
		if (!isExternalNode) {
			Vector<Double> answer = getInfoFromInsideNodes(node);
			node.object = new Obj(answer.get(0), answer.get(1), answer.get(2));
			if (node.fatherNode != null) {
				recalculateIntenalNode(node.fatherNode);
			}
		}

	}


	public void addObject(Node root, Node node, Obj object) {
		if (node.isExternalNode) {
			if (!node.isFill) {
				node.isFill = node.isrealObject = true;
				node.object = object;
				if(node.fatherNode !=null)
					recalculateIntenalNode(node.fatherNode);
			} else {
				node.createSectors(root, node);
				node.isrealObject = false;
				int bestQuad = searchBestQuadrant(node, node.object);
				node.addObject(root, node.insideNodes.get(bestQuad), node.object);

				bestQuad = searchBestQuadrant(node, object);
				addObject(root, node.insideNodes.get(bestQuad), object);

				Vector<Double> answer = getInfoFromInsideNodes(node);
				node.object = new Obj(answer.get(0), answer.get(1), answer.get(2));

			}

		} else {// if is intern node
			int bestQuad = searchBestQuadrant(node, object);
			addObject(root, node.insideNodes.get(bestQuad), object);
			Vector<Double> answer = getInfoFromInsideNodes(node);
			node.object = new Obj(answer.get(0), answer.get(1), answer.get(2));

		}
	}

	
}
